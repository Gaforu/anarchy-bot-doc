# Gaforu Anarchy Bot Public Help Page

&nbsp;&nbsp;<b> (c) 2023 https://github.com/Gaforu </b>  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All rights reserved.  

---

## Available Commands:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>All commands have a 3 seconds cooldown. Everything sent during the cooldown is ignored.</u>  

 * ### General
   `$help` Prints a link leading to this page.  
   `$tps` Estimated TPS.  
   `$ping <player>` Ping of a player in milliseconds.  
   `$lowestping` The player currently with lowest ping in milliseconds.  
   `$highestping` The player currently with highest ping in milliseconds.  
   `$uuid <player>` UUID of an online player.  
   `$gametime`  Current in-game time on the server.  
   `$coords` Current position of the bot instance.  
   `$kill` Make the bot do `/kill`.  

 * ### Time Util
   `$time <country>` Current time in a country.  
   `$gmt <offset>` Current time in a timezone.  

 * ### Geolocation Util
   `$geoip <ip>` Get the geolocation of an IP address.   

 * ### Cryptographic Util
   <u>All commands under this category have a cooldown of 10 seconds instead of 3 due to anti-spam.</u>  
   `$hash md5 <string>` MD5 hash of a string.  
   `$hash sha1 <string>` SHA1 hash of a string.  
   `$hash sha256 <string>` SHA256 hash of a string.  
   `$hash sha512 <string>` SHA512 hash of a string.  
  
   `$base64 <string>` Encode a string using base64. <i>Decoding is not available to the public due to "hygiene" issues.</i>  

 * ### PRNG Util
   `$rand bool` Random boolean value.  
   `$rand int` Random 32-bit signed integer.  
   `$rand int <upper>` Random integer between `0` (inclusive) and `upper` (exclusive). The value of `upper` must be positive and may not exceed 16777216.  
   `$rand int <lower> <upper>` Random integer between `lower` (inclusive) and `upper` (exclusive). The value `upper` - `lower` must be positive and may not exceed 16777216.  
   `$rand float` Random floating point number between `0` (inclusive) and `1` (exclusive).  
   `$rand float <upper>` Random floating point number between `0` (inclusive) and `upper` (exclusive). The value of `upper` must be positive.  
   `$rand float <lower> <upper>` Random floating point number between `lower` (inclusive) and `upper` (exclusive). The value `upper` - `lower` must be positive.  
   `$rand gaussian` Random floating point number following the normal distribution with <i>μ</i> = 0 and <i>σ</i> = 1.  
   `$rand gaussian <mu> <sigma>` Random floating point number following the normal distribution with specified <i>μ</i> and <i>σ</i>.  

 * ### Basic Math Functions Util
   `$math exp <x>` e<sup><i>x</i></sup>  
   `$math exp10 <x>` 10<sup><i>x</i></sup>  
   `$math log <x>` log<sub>e</sub>(<i>x</i>)  
   `$math log2 <x>` log<sub>2</sub>(<i>x</i>)  
   `$math log10 <x>` log<sub>10</sub>(<i>x</i>)  
   `$math sqrt <x>` Square root of <i>x</i>  
   `$math cbrt <x>` Cube root of <i>x</i>  
   `$math sin <x>` sin(<i>x</i>)  
   `$math cos <x>` cos(<i>x</i>)  
   `$math tan <x>` tan(<i>x</i>)  
   `$math asin <x>` sin<sup>-1</sup>(<i>x</i>)  
   `$math acos <x>` cos<sup>-1</sup>(<i>x</i>)  
   `$math atan <x>` tan<sup>-1</sup>(<i>x</i>)  
   `$math sinh <x>` sinh(<i>x</i>)  
   `$math cosh <x>` cosh(<i>x</i>)  
   `$math tanh <x>` tanh(<i>x</i>)  
   `$math asinh <x>` sinh<sup>-1</sup>(<i>x</i>)  
   `$math acosh <x>` cosh<sup>-1</sup>(<i>x</i>)  
   `$math atanh <x>` tanh<sup>-1</sup>(<i>x</i>)  
  
   `$calc <expr>` Evaluate a piece of math expression.  

 * ### Misc
   `$dice <count>` Roll some dice. You may throw up to 256 dice at once.  
   `$poker` Draw a random poker card.  
   `$spin` [Play slot machine game with CCP betting on your social credits.](./SlotMachine.md)  
   `$email <receiver@example.com> # <subject> # <message (text/html)>` Send an email to the specified address.  
   &nbsp;&nbsp;**NOTE**: Certain vulgar wards may not pass the server-side checks, which I have no control of.  

 * ### Redacted
   `<Redacted for public safety>` Something that inevitably spreads everywhere on an anarchy server. Hint: `int D = 8;`  
