# Social Credit Slot Machine  

&nbsp;&nbsp;<b> (c) 2023 https://github.com/Gaforu </b>  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All rights reserved.  
&nbsp;  
&nbsp;&nbsp;Play slot machine game with Chinese Communist Party betting on your social credits.  
 * <i><b>Warning:</b> You will be sent to jail if you lost all your social credits!</i>  

---

### Max Bet:
 * <b>1000 social credits.</b> Betting over 1000 credits will be noticed by the CCP and result in your social credit score deduced by 100 immediately!

---

### Balance Query:
 * <i>Chinese government does not authorize citizens to perform such action.</i>  

---

### Slot Pool:
 * <b>`[0-9A-Z\@\#\$\%]`</b> Anything matching this regex.

---

### Prizes:  
 * <b>Triple</b>: All 3 slots land on the same value.  
   * <b>Alphabets</b>: <u>100x</u> (e.g. `AAA`)  
   * <b>Numbers</b>: <u>200x</u> (e.g. `111`)  
   * <b>Symbols</b>: <u>500x</u> (e.g. `###`)  
 * <b>Staircase</b>: 3 slots form a sequences with increment of 1.  
   * <b>Alphabets/Numbers</b>: <u>75x</u> (e.g. `345`, `JKL`)  
 * <b>Reversed-staircase</b>: 3 slots form a sequences with decrement of 1.  
   * <b>Alphabets/Numbers</b>: <u>50x</u> (e.g. `543`, `QPO`)  
 * <b>Vocabulary</b>: 3 slots form a common word.  
   * <b>Alphabets</b>: <u>10x</u> (e.g. `ZOO`)  
 * <b>Palindrome</b>:  
   * <b>Alphabets</b>: <u>5x</u> (e.g. `OWO`)  
   * <b>Alphabets</b> + <b>Numbers</b>: <u>1x</u> (e.g. `5O5`, `0O0`)  
   * <b>Alphabets</b> (<i>x2</i>) + <b>Symbols</b> (<i>x1</i>): <u>10x</u> (e.g. `O#O`)  
   * <b>Alphabets</b> (<i>x1</i>) + <b>Symbols</b> (<i>x2</i>): <u>15x</u> (e.g. `@W@`)  
   * <b>Numbers</b>: <u>20x</u> (e.g. `414`)  
   * <b>Numbers</b> + <b>Symbols</b>: <u>25x</u> (e.g. `0#0`, `$0$`)  
   * <b>Symbols</b>: <u>75x</u> (e.g. `$#$`)  
 * <b>Double</b>: 2 out of 3 slots land on an identical value.  
   * <b>Alphabets</b>: <u>1x</u> (e.g. `AAS`)  
   * <b>Alphabets</b> (<i>x1</i>) + <b>Symbols</b> (<i>x2</i>): <u>2x</u> (e.g. `@@S`)  
   * <b>Numbers</b>: <u>10x</u> (e.g. `007`)  
   * <b>Numbers</b> + <b>Symbols</b>: <u>15x</u> (e.g. `44$`, `4$$`)  
   * <b>Symbols</b>: <u>50x</u> (e.g. `@$$`)  
 * <b>Group</b>: 3 slots land in a specific group.  
   * <b>Alphabet</b> + <b>Number</b> + <b>Symbol</b>: <u>1x</u> (e.g. `4$S`)  
   * <b>Alphabets</b> (<i>x1</i>) + <b>Symbols</b> (<i>x2</i>): <u>1x</u> (e.g. `@$S`)  
   * <b>Numbers</b>: <u>5x</u> (e.g. `609`)  
   * <b>Numbers</b> (<i>x2</i>) + <b>Symbols</b> (<i>x1</i>): <u>1x</u> (e.g. `4$5`)  
   * <b>Numbers</b> (<i>x1</i>) + <b>Symbols</b> (<i>x2</i>): <u>2x</u> (e.g. `@5$`)  
   * <b>Symbols</b>: <u>25x</u> (e.g. `@#$`)  
 * <b>Known Special</b>: Special combinations.
   * `ONE`: <u>11x</u>  
   * `TWO`: <u>22x</u>  
   * `SIX`: <u>66x</u>  
   * <i>`Hidden entries`</i>: <u>???x</u>
 * <b><i>Landmines</i></b>: 3 slots land on certain string that the Chinese Communist Party deems disruptive. <i>Avoid them!</i>  
   * <b><i>`Entries redacted by Chinese government.`</i></b>  

&nbsp;&nbsp;&nbsp;&nbsp;<b>Note</b>: All prizes, unless explicitly stated, do not stack.  
